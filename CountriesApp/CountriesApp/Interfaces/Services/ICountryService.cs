﻿using CountriesApp.Interfaces.Models;
using System.Collections.Generic;

namespace CountriesApp.Interfaces.Services
{
    public interface ICountryService
    {
        ICollection<ICountry> GetCountries();
        ICollection<ICountry> SaveCountry(ICountry country);
    }
}
