﻿using CountriesApp.Models.Xml;

namespace CountriesApp.Interfaces.Models
{
    public interface ICountries
    {
        Country[] Entries { get; set; }
    }
}
