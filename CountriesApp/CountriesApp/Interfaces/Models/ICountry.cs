﻿namespace CountriesApp.Interfaces.Models
{
    public interface ICountry
    {
        string Code { get; set; }
        string Name { get; set; }
        string Capital { get; set; }
    }
}
