﻿using CountriesApp.Exceptions;
using CountriesApp.Interfaces.Models;
using System.ComponentModel.DataAnnotations;

namespace CountriesApp.ViewModels
{
    public class CountryViewModel : CountrySummaryViewModel
    {
        [Required]
        [StringLength(30)]
        public string Capital { get; set; }

        public CountryViewModel() { }

        public CountryViewModel(ICountry country)
        {
            if (country == null)
                throw new CountryNotFoundException();

            this.Capital = country.Capital;
            base.Code = country.Code;
            base.Name = country.Name;
        }
    }
}
