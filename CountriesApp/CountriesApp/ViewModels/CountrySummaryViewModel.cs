﻿using System.ComponentModel.DataAnnotations;

namespace CountriesApp.ViewModels
{
    public class CountrySummaryViewModel
    {
        private string _code;

        [Required]
        [StringLength(2)]
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                value = value ?? "";
                _code = value.ToUpper();
            }
        }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }
    }
}
