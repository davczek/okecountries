﻿using CountriesApp.Interfaces.Models;
using System.Collections.Generic;
using System.Linq;

namespace CountriesApp.ViewModels
{
    public class CountriesViewModel
    {
        public CountrySummaryViewModel[] CountriesSummaries { get; set; }

        public CountriesViewModel(ICollection<ICountry> countries)
        {
            if (countries != null)
            {
                this.CountriesSummaries = new CountrySummaryViewModel[countries.Count];
                for (var i = 0; i < countries.Count; i++)
                {
                    var country = countries.ElementAt(i);
                    this.CountriesSummaries[i] = new CountrySummaryViewModel
                    {
                        Code = country.Code,
                        Name = country.Name
                    };
                }
            }
            else
                CountriesSummaries = new CountrySummaryViewModel[] { };
        }
    }
}
