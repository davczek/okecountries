﻿function showCountryDetail(countryCode) {
    $.get('/Home/CountryDetails?code=' + countryCode, function (data) {
        $('#country-details').html(data);
    }); 
}

function showNewCountryModal() {
    $.get('/Home/AddCountry', function (data) {
        $('#add-country-form').html(data);
        $('#add-country-modal').modal();
    }); 
}
