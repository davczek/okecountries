﻿using CountriesApp.Interfaces.Models;
using System.Xml.Serialization;

namespace CountriesApp.Models.Xml
{
    [XmlRoot(ElementName = "countries")]
    public class Countries : ICountries
    {
        [XmlElement(ElementName = "country")]
        public Country[] Entries { get; set; }
    }
}
