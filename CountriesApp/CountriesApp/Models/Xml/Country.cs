﻿using CountriesApp.Interfaces.Models;
using System.Xml.Serialization;

namespace CountriesApp.Models.Xml
{
    [XmlRoot("country")]
    public class Country : ICountry
    {
        [XmlAttribute("code")]
        public string Code { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("capital")]
        public string Capital { get; set; }
    }
}
