﻿using System;
using System.Collections.Generic;
using CountriesApp.Interfaces.Models;
using CountriesApp.Interfaces.Services;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Xml.Serialization;
using CountriesApp.Models.Xml;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Xml;

namespace CountriesApp.Services
{
    public class CountryService : ICountryService
    {
        private readonly string _dataPath;
        private readonly ILogger _logger;

        public CountryService(IHostingEnvironment env,
            ILogger<ICountryService> logger)
        {
            _dataPath = Path.Combine(env.WebRootPath, @"data\countries.xml");
            _logger = logger;
        }

        public ICollection<ICountry> GetCountries()
        {
            try
            {
                var xmlSer = new XmlSerializer(typeof(Countries));
                using (var fs = new FileStream(_dataPath, FileMode.Open))
                {
                    var countries = (xmlSer.Deserialize(fs) as Countries);
                    return countries.Entries;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError("Error occured while getting countries. Error message: {message}", ex.Message);
                return null;
            }
        }

        public ICollection<ICountry> SaveCountry(ICountry country)
        {
            try
            {
                var countriesList = GetCountries()?.ToList();
                if (countriesList == null)
                    countriesList = new List<ICountry>();
                countriesList.Add(country);
                var countries = new Countries
                {
                    Entries = countriesList
                                    .Select(c => { return (Country)c; })
                                    .ToArray()
                };

                var xmlSer = new XmlSerializer(typeof(Countries));
                using (var fs = new FileStream(_dataPath, FileMode.Open))
                {
                    xmlSer.Serialize(fs, countries);
                    return countries.Entries;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Error occured while saving country. Error message: {message}", ex.Message);
                return null;
            }
        }
    }
}
