﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CountriesApp.Interfaces.Services;
using CountriesApp.ViewModels;
using CountriesApp.Interfaces.Models;
using System.Linq;
using System;
using CountriesApp.Models.Xml;

namespace CountriesApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICountryService _countryService;
        private readonly ICollection<ICountry> _countries;

        public HomeController(ICountryService countryService)
        {
            _countryService = countryService;
            _countries = _countryService.GetCountries();
        }

        public IActionResult Index()
        {
            var vm = new CountriesViewModel(_countries);
            return View(vm);
        }

        public IActionResult CountryDetails(string code)
        {
            var country = _countries.FirstOrDefault(c => c.Code.Equals(code, StringComparison.CurrentCultureIgnoreCase));
            var vm = new CountryViewModel(country);
            return PartialView("~/Views/Partials/CountryDetails.cshtml", vm);
        }

        [HttpGet]
        public IActionResult AddCountry()
        {
            return PartialView("~/Views/Partials/AddCountryModal.cshtml", new CountryViewModel());
        }

        [HttpPost]
        public IActionResult AddCountry(CountryViewModel model)
        {
            var country = new Country
            {
                Code = model.Code,
                Capital = model.Capital,
                Name = model.Name
            };
            var countries = _countryService.SaveCountry(country);
            return View("Index", (new CountriesViewModel(countries)));
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
